"
"  Felipe Freire - felbs
"  https://github.com/felbs
"  https://gitlab.com/Felbs
"


set number 
set numberwidth=1

set wildmenu
set nobackup
set noswapfile

syntax enable


imap ii <ESC>
