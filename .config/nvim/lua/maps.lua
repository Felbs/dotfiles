local function map(m, k, v)
        vim.keymap.set(m, k, v, {noremap = true})
end


map("i", "ii", "<ESC>")
