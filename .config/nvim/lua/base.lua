local g = vim.g
local o = vim.o
local A = vim.api


--o.termguicolors = true

-- Better editor UI
o.number = true
o.numberwidth = 2
--o.signcolumn = "yes"


-- Better editing exp
--o.expandtab = true
--o.smarttab = true
--o.cindent = true
--o.autoident = true
o.shiftwidth = 2
o.tabstop = 2
--o.textwidth = 300


A.nvim_set_keymap('i', 'ii', '<ESC>', {noremap = true})
