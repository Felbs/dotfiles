##################################
### Felbs bashrc configuration ###
##################################

export PS1="\033[1m$USER@hurin --> \033[m"

## felbs scripts

#. $HOME/.bantex.sh
. $HOME/.scripts/workon/workon.sh
. $HOME/.scripts/xerg/xerg.sh
. $HOME/.scripts/jj/jj.sh
. $HOME/.scripts/archive/archive.sh

. "$HOME/.cargo/env"

## felbs aliases

# -- OS
alias sdw="shutdown now"
alias boo="systemctl reboot"

# -- Fs and Editing
alias ls='ls --color=auto'
alias ll='ls -lav --ignore=..'   # show long listing of all except ".."

alias nv="nvim"
alias wkn="workon -n"

# volume up/down (pipewire)
alias vup="pactl set-sink-volume 0 +10%"
alias vdown="pactl set-sink-volume 0 -10%"

## exports

export PATH=$PATH:$HOME/.local/bin
