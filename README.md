# Felipe Freire dotfiles
#
# dotfiles are configuration files. These are my dotfiles!
# Feel free to download, use, modify it


.bashrc -> configuration file for bash command line.
.config -> a folder containing a ton of configuration
.Xresources -> configuration for Xwindow systems
.vimrc -> configuration file for vim text editor

